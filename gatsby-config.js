require("dotenv").config()
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `End's Reach - 3D Exploration Platformer for the Atari VCS`,
    description: `The official site of End's Reach, a 3D Exploration Platformer for the Atari VCS by Cuirass Entertainment. Wield four legendary weapons, explore four unique planets, and unravel the mysteries within.`,
    image: `https://endsreach.com/images/siteImage.png`,
    twitterUsername: `@CuirassSocial`,
    siteUrl: `https://endsreach.com`,
    keywords: `end's reach, ends reach, atari, vcs, atari vcs, indie, platformer, video game, game, 3D, adventure, action adventure, cuirass entertainment, cuirass`,
  },
  plugins: [
    {
      resolve: "gatsby-source-sanity",
      options: {
        projectId: process.env.SANITY_PROJECT_ID,
        dataset: process.env.SANITY_PROJECT_DATASET,
        token: process.env.SANITY_READ_TOKEN,
      },
    },
    'gatsby-plugin-remove-console',
    "gatsby-plugin-sharp",
    "gatsby-plugin-image",
    "gatsby-transformer-sharp",
    "gatsby-plugin-vanilla-extract",
    'gatsby-plugin-postcss',
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        output: '/',
      }
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://endsreach.com',
        sitemap: 'https://endsreach.com/sitemap-0.xml',
        policy: [{userAgent: '*', allow: '/'}]
      }
    },
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "End's Reach - 3D Exploration Platformer for the Atari VCS",
        short_name: "End's Reach",
        start_url: "/",
        background_color: "#000000",
        theme_color: "#769C93",
        icon: "src/favicons/favicon-32x32.png",
        icons: [
          {
            src: `src/favicons/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `src/favicons/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
          {
            src: `src/favicons/favicon-16x16.png`,
            sizes: `16x16`,
            type: `image/png`,
          },
          {
            src: `src/favicons/favicon-32x32.png`,
            sizes: `32x32`,
            type: `image/png`,
          },
        ]
      },
    },
  ],
}
