module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
      extend: {
        colors: {
          brand: {
            text: "#000000",
            primary: "#213742",
            secondary: "#285659",
            muted: "#0D332F",
            secondaryMuted: "#0D2C33",
            active: "#244F46",
            black: "#000",
            tertiary: '#159BF3',
            lowlight: '#004CA3',
            highlight: '#BC027F',
            background: "#0D332F",
          }
        },
      }
    },
    plugins: []
  };