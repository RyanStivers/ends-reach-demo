
# Project Title

End's Reach Official Site - Cuirass Entertainment


## Acknowledgements
 - [Ryan Stivers](https://stivers.io)
 - [Gatsby](https://www.gatsbyjs.com/)
 - [Sanity](https://www.sanity.io/)
 - [Three.js](https://threejs.org/)
 - [Cloudflare](https://www.cloudflare.com/)
 - [Tailwind](https://tailwindcss.com/)
 - [Vanilla Extract](https://vanilla-extract.style/)



## Authors

- [@RyanStivers](https://gitlab.com/RyanStivers)


## Run Locally

1. **Set up Sanity Studio**

   1. In the `studio` directory, install dependencies for Sanity Studio:

      ```sh
      yarn
      ```

   2. Create a new Sanity project by running:

      ```sh
      yarn sanity-init
      ```

      This will prompt you to log in if this is your first time using Sanity CLI.

   3. Deploy the Sanity GraphQL API for your new project:

      ```sh
      yarn deploy
      ```

   4. Start the Sanity Studio development server to start editing content:

      ```sh
      yarn start
      ```

   5. In your _Gatsby site's directory_, to create `.env.development` and `.env.production` files with configuration for your Sanity project, run:

      ```sh
      yarn & yarn setup
      ```

2. **Start developing**

   In your site directory, start the development server:

   ```sh
    gatsby develop
   ```

   Your site should now be running at <http://localhost:8000>

3. **Open the source code and start editing**

