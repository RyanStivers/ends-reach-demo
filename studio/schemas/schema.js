// First, we must import the schema creator
import createSchema from "part:@sanity/base/schema-creator"

// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type"

import homepage from "./homepage"
import spaceBanner from "./spaceBanner"
import link from "./link"
import hero from "./hero"
import feature from "./feature"
import features from "./features"
import callToAction from "./callToAction"
import testimonial from "./testimonial"
import testimonials from "./testimonials"
import stat from "./stat"
import stats from "./stats"
import keyPoint from "./keyPoint"
import keyPoints from "./keyPoints"
import gallery from "./gallery"
import navItem from "./navItem"
import navItemGroup from "./navItemGroup"
import socialLink from "./socialLink"
import layoutHeader from "./layoutHeader"
import layoutFooter from "./layoutFooter"
import layout from "./layout"
import page from "./page"




// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "default",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    homepage,
    layoutHeader,
    spaceBanner,
    hero,
    keyPoint,
    keyPoints,
    feature,
    features,
    callToAction,
    testimonial,
    testimonials,
    stat,
    stats,
    gallery,
    navItem,
    navItemGroup,
    link,
    socialLink,
    layoutFooter,
    layout,
    page,
  ]),
})
