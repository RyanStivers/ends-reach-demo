export default {
  name: "stats",
  title: "Stats",
  type: "document",
  fields: [
    { title: "Heading", name: "heading", type: "string" },
    { title: "Kicker", name: "kicker", type: "string" },
    { title: "Text", name: "text", type: "string" },
    { title: "Image", name: "image", type: "image" },
    { title: "Icon", name: "icon", type: "image" },
    {
      title: "Content",
      name: "content",
      type: "array",
      of: [
        {
          type: "reference",
          to: [{ type: "stat" }],
        },
      ],
    },
    {
      title: "Links",
      name: "links",
      type: "array",
      of: [
        {
          type: "reference",
          to: [{ type: "link" }],
        },
      ],
    },
  ],
}
