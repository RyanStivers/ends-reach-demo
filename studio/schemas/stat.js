export default {
  name: "stat",
  title: "Stat",
  type: "document",
  fields: [
    { title: "Heading", name: "heading", type: "string" },
    { title: "Value", name: "value", type: "string" },
    { title: "Label", name: "label", type: "string" },
  ],
}
