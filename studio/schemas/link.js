export default {
  name: "link",
  title: "Link",
  type: "document",
  fields: [
    { title: "Text", name: "text", type: "string" },
    { title: "href", name: "href", type: "string" },
  ],
}
