export default {
    name: "gallery",
    title: "Image Gallery",
    type: "document",
    fields: [
        { 
            title: "Images", 
            name: "images", 
            type: "array",
            options: {
                layout: 'grid'
            },
            of: [{
                type: 'image'
            }]
        }
    ]
}