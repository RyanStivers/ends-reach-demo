import { globalStyle, globalKeyframes, globalFontFace, style } from "@vanilla-extract/css"
import { theme } from "./theme.css"


globalStyle("body", {
  margin: 0,
  fontFamily: theme.fonts.text,
  color: theme.colors.text,
  backgroundColor: theme.colors.background,
  WebkitFontSmoothing: "antialiased",
  MozOsxFontSmoothing: "grayscale",
})

globalStyle("*", {
  boxSizing: "border-box",
})

globalStyle("img", {
  borderRadius: theme.radii.button,
})

globalKeyframes("zoomInUp", {
  "0%": {
    transform: "scale(0.95) translateY(10px) translateX(-50%)",
    visibility: "hidden",
    opacity: 0,
  },
  "100%": {
    opacity: 1,
    transform: "scale(1), translateY(0) translateX(-50%)",
    visibility: "visible",
  },
})

globalKeyframes("zoomOutDown", {
  "0%": {
    transform: "scale(1) translateY(0) translateX(-50%)",
    opacity: 1,
  },
  "100%": {
    opacity: 0,
    transform: "scale(0.95) translateY(10px) translateX(-50%)",
    visibility: "hidden",
  },
})

globalKeyframes("fadeIn", {
  "0%": {
    visibility: "hidden",
    opacity: 0,
  },
  "100%": {
    opacity: 1,
    visibility: "visible",
  },
})

globalKeyframes("fadeOut", {
  "0%": {
    opacity: 1,
  },
  "100%": {
    opacity: 0,
    visibility: "hidden",
  },
})

const exo = 'GlobalExo';
const exoLight = 'GlobalExoLight'

globalFontFace(exo, {
  src: 'url(/fonts/Exo-Regular.woff2) format("woff2")',
  fontDisplay: "swap",
});

globalFontFace(exoLight, {
  src: 'url(/fonts/Exo-Light.woff2) format("woff2")',
  fontDisplay: "swap",
});

export const font = style({
  fontFamily: exo + ", sans-serif",
  fontSize: "18px"
});
