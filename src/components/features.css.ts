import { style } from "@vanilla-extract/css"

export const glass = style({
  background: "rgba(13,44,51,0.36)",
  borderRadius: "16px",
  boxShadow: "0px 7px 12px 3px rgba(0,0,0,0.1)",
  backdropFilter: "blur(11px)",
  WebkitBackdropFilter: "blur(11px)",
  border: "1px solid rgb(13 44 51 / 42%)",
})