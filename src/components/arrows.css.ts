import { keyframes, style } from "@vanilla-extract/css"

const arrowBlink = keyframes({
    '0%': {opacity: '0'},
    '40%': {opacity: '1'},
    '80%': {opacity: '0'},
    '100%': {opacity: '0'}
});

export const blink = style({
    animation: "arrowBlink 2s infinite",
    WebkitAnimation: "arrowBlink 2s infinite",
    
});

export const arrows = style({
    width: "60px",
    height: "72px",
    margin: "auto",
    // position: "absolute",
    // left: "50%",
    // marginLeft: "-30px",
    // bottom: "125px"
});

export const a1 = style({
    stroke: "#2994D1",
    fill: "transparent",
    strokeWidth: "1px",  
    animationName: arrowBlink,
    WebkitAnimationName: arrowBlink,
    animationDuration: "2s",
    WebkitAnimationDuration: "2s",
    animationDelay: "-1s",
    WebkitAnimationDelay: "-1s",
    animationIterationCount: "infinite",
    WebkitAnimationIterationCount: "infinite",
});

export const a2 = style({
    stroke: "#2994D1",
    fill: "transparent",
    strokeWidth: "1px",  
    animationName: arrowBlink,
    WebkitAnimationName: arrowBlink,
    animationDuration: "2s",
    WebkitAnimationDuration: "2s",
    animationDelay: "-0.5s",
    WebkitAnimationDelay: "-0.5s",
    animationIterationCount: "infinite",
    WebkitAnimationIterationCount: "infinite",
});

export const a3 = style({
    stroke: "#2994D1",
    fill: "transparent",
    strokeWidth: "1px",
    animationName: arrowBlink,
    WebkitAnimationName: arrowBlink,
    animationDuration: "2s",
    WebkitAnimationDuration: "2s",
    animationDelay: "0s",
    WebkitAnimationDelay: "0s",
    animationIterationCount: "infinite",
    WebkitAnimationIterationCount: "infinite",
});

