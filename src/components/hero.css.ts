import { style } from "@vanilla-extract/css"
import { theme } from "../theme.css"

export const heroSection = style({
    backgroundColor: theme.colors.secondaryMuted,
    paddingTop: theme.space[6],
    paddingBottom: theme.space[6],
    paddingLeft: theme.space[4],
    paddingRight: theme.space[4],
    background: "linear-gradient(rgb(10 1 7 / 59%),rgb(28 4 18 / 89%),rgb(10 0 51 / 89%)),url(/images/blackhole.webp) no-repeat right/cover",
    // background: "transparent"
    clipPath: "polygon(0 14%, 100% 0%, 100% 86%, 0% 100%)",
    color: theme.colors.text
})