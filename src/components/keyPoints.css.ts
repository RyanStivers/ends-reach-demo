import { style } from "@vanilla-extract/css"

export const keyPointFlex = style({
    alignContent: "center",
    alignItems: "center",
})

export const keyPointFlexItem = style({
    flex: "1"
})
