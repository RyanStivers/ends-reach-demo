import * as React from "react"
import { Helmet } from "react-helmet"

export default function Head({ title, description, image }) {
  return (
    <>
      <Helmet htmlAttributes={{ lang: 'en-US' }} />
    </>
  )
}
