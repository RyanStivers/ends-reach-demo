import { style } from "@vanilla-extract/css"
import { theme } from "../theme.css"

export const galleryImage = style({
    objectFit: "cover",
    maxWidth: "100%",
    height: "auto",
    verticalAlign: "middle",
    borderRadius: "5px",
    cursor: "pointer"
    
})

export const glass = style({
    backdropFilter: "blur(16px) saturate(180%)",
    WebkitBackdropFilter: "blur(16px) saturate(180%)",
    backgroundColor: "rgba(36, 79, 70, 0.75)",
    borderRadius: "12px",
    border: "1px solid rgba(255, 255, 255, 0.125)"
  })

export const galleryWrapper = style({
    display: "grid",
    gridTemplateColumns: "repeat(3, 1fr)",
    gridColumnGap: theme.space[2],
    gridRowGap: theme.space[2],
})

export const galleryContainer = style({
    
})

export const gallerySection = style({
    // clipPath: 'polygon(0 0%, 100% 5%, 100% 100%, 0% 94%)',
    // background: theme.colors.secondaryMuted
    background: "linear-gradient(rgb(1 10 5 / 79%),rgb(34 69 57 / 69%),rgb(5 3 37 / 84%)),url(/images/2.webp) no-repeat top /cover"
})