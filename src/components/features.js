import * as React from "react"
import { graphql } from "gatsby"
import { Container, Box, Kicker, Heading, Text } from "./ui"
import Feature from "./feature"
import * as styles from "./features.css"

export default function Features(props) {
  return (
    <Container width="fullbleed" id={props.heading}>
      <Box className={styles.glass} background="muted" radius="large">
        <Box center paddingY={5}>
          <Heading>
            {props.kicker && <Kicker>{props.kicker}</Kicker>}
            {props.heading}
          </Heading>
          {props.text && <Text>{props.text}</Text>}
        </Box>
        {props.content.map((feature, i) => (
          <Feature key={feature.id} {...feature} flip={Boolean(i % 2)} />
        ))}
      </Box>
    </Container>
  )
}

export const query = graphql`
  fragment FeaturesContent on Features {
    id
    kicker
    heading
    text
    content {
      id
      ...FeatureContent
    }
  }
`
