import * as React from "react"
import Footer from "./footer"
import Head from "./head"
import "../styles.css"

const Layout = (props) => {
  return (
    <>
      <Head {...props} />
      {props.children}
      <Footer />
    </>
  )
}

export default Layout
