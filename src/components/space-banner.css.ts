import { fontFace, style } from '@vanilla-extract/css';

//P&C
const oneDay = fontFace({
    src: 'url(/fonts/ONEDAY.woff2) format("woff2")',
    fontDisplay: "swap",
});
const exo = fontFace({
    src: 'url(/fonts/Exo-Regular.woff2) format("woff2")',
    fontDisplay: "swap",
});
const exoLight = fontFace({
    src: 'url(/fonts/Exo-Light.woff2) format("woff2")',
    fontDisplay: "swap",
});
  

export const splashTitle = style({
    fontFamily: oneDay + ", sans-serif",
    fontSize: "8em",
    fontWeight: "unset",
    textAlign: "center",
    '@media': {
        'screen and (max-width: 800px)': {
            fontSize: "6em"
        },
        'screen and (max-width: 370px)': {
            fontSize: "4em"
        },
        'screen and (max-width: 300px)': {
            fontSize: "3em"
        }
    }
});

export const splashSub = style({
    fontFamily: exo + ", sans-serif",
    fontSize: "18px",
    textAlign: "center",
});

export const titleWrapper = style({
    textAlign: "center"
});

export const arrowWrapper = style({
    
});