import { style } from "@vanilla-extract/css"
import { theme } from "../theme.css"

export const slanted = style({
  backgroundColor: theme.colors.secondaryMuted,
  paddingTop: theme.space[1],
  paddingBottom: theme.space[1],
  paddingLeft: theme.space[4],
  paddingRight: theme.space[4],
  // clipPath: 'polygon(0 14%, 100% 0%, 100% 86%, 0% 100%)',
  // background: "linear-gradient(rgb(3 1 10 / 94%),rgb(38 34 69 / 67%),rgb(13 4 53 / 81%)),url(/images/blackhole.png) no-repeat left  top/cover"
  background: "transparent"
})
