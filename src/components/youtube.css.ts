import { globalStyle, style } from "@vanilla-extract/css"
import { theme } from "../theme.css"

export const youtubeContainer = style({
  position: "relative",
  paddingBottom: "56.25%",
});

export const slanted = style({
  paddingTop: theme.space[1],
  paddingBottom: theme.space[1],
  paddingLeft: theme.space[4],
  paddingRight: theme.space[4],
})

globalStyle("iframe", {
  position: "absolute",
  top: "0",
  left: "0",
  width: "100%",
  height: "100%",
});
