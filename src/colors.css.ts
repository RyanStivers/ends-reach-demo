export const colors = {
  background: "#0D332F",
  text: "#769C93",
  active: "#244F46",
  primary: "#769C93",
  muted: "#244F46",
  secondary: "#13404b",
  secondaryMuted: "#0D2C33",
  tertiary: "#2A6266",
  tertiaryMuted: "#264A5C",
  black: "#000",
  kicker: "palegoldenrod",
}
